/* eslint-disable no-useless-escape */
/* eslint-disable react/prop-types */
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';


const useStyles = makeStyles({
  root: {
    maxWidth: 345,
    margin: "10px",
  },
  card: {
    transform: "rotate(180deg)"
  }
});


function TarotCard({name, img, keywords}) {
  
  const rand = Math.random()
  const classes = useStyles();

  if (name && img) {
    const cap = keywords.map(keyword => 
      keyword.toUpperCase()
    )
    console.log(cap)
    return (
    <Card className={classes.root}>
        <CardActionArea>
          {rand > 0.5 && 
          <CardMedia
            component="img"
            alt={name}
            height="600"
            src={process.env.PUBLIC_URL + '/images/' + img}
            title={name}
            className={classes.card}
          />}
          {rand <= 0.5 &&
          <CardMedia
            component="img"
            alt={name}
            height="600"
            src={process.env.PUBLIC_URL + '/images/' + img}
            title={name}
          />
          }
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              {name}
            </Typography>
            <Typography variant="body2" component="p">
              {cap.slice(0,2).toString()}
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    );
  } else {
    return (
      <Card className={classes.root}>
        <CardActionArea>
          <CardMedia
            component="img"
            alt={name}
            height="600"
            src={process.env.PUBLIC_URL + '/images/blank.jpg'}
            title={name}
          />
        </CardActionArea>
      </Card>
    );
  }

  
}

export default TarotCard