const graphql = require('graphql')
const {GraphQLObjectType, GraphQLSchema, GraphQLInt, GraphQLString, GraphQLList} = graphql
const data = require("../data/tarot-images.json")
const TarotType = require('./TypeDefs/TarotType')

const RootQuery = new GraphQLObjectType({
    name: "RootQueryType",
    fields: {
        getAllTarot: {
            type: new GraphQLList(TarotType),
            args: {
                id: {
                    type: GraphQLString
                }
            },
            resolve(parent, args) {
                return data.cards
            },
        },
        getRandomTarot: {
            type: new GraphQLList(TarotType),
            args: {
                num: {
                    type: GraphQLInt
                }
            },
            resolve(parent, args) {
                var res = []
                while (res.length < args.num) {
                    var card = data.cards[Math.floor(Math.random() * data.cards.length)]
                    if (!res.includes(card)){
                        res.push(card)
                    }
                }
                return res
            }
        }
    }
})

const Mutation = new GraphQLObjectType({
    name: "Mutation",
    fields: {
        createTarot: {
            type: TarotType,
            args: {
                name: {type: GraphQLString},
                number: {type: GraphQLInt},
                arcana: {type: GraphQLString},
                suit: {type: GraphQLString},
                img: {type: GraphQLString},
            },
            resolve(parent, args) {
                data.push({
                    name: args.name,
                    number: args.number,
                    arcana: args.arcana,
                    suit: args.suit,
                    img: args.img,
                })
                return args
            }
        }
    }
})

module.exports = new GraphQLSchema({query: RootQuery, mutation: Mutation})