import React, {useState, useEffect} from 'react';
import {ApolloClient, InMemoryCache, ApolloProvider, HttpLink, from, useQuery, gql} from "@apollo/client"
import {onError} from "@apollo/client/link/error"

import TarotCards from "./Components/TarotCard"
import Panel from "./Components/Panel"

import { makeStyles } from '@material-ui/core/styles';
import {Button} from '@material-ui/core';


const errorLink = onError(({graphqlErrors, networkError}) => {
  if (graphqlErrors) {
    graphqlErrors.map(({message, location, path}) => {
      alert(`Graphql error ${message}`)
    })
  }
})

const link = from([
  errorLink,
  new HttpLink({ 
    uri: "http://localhost:6969/graphql",
    // fetchOptions: {
    //   mode: 'no-cors'
    // } 
  })
])

const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: link
})

const useStyles = makeStyles((theme) => ({
  root: {
    display: "grid",
    backgroundColor: "#222",
    // width: "100%",
    height:"100%",
    [theme.breakpoints.down('sm')] : {
      gridTemplateRows: "7fr 3fr",
      gridTemplateAreas:
      `
      "board"
      "panel"
      `
    },
    [theme.breakpoints.up('md')]: {
      gridTemplateColumns: "1fr 9fr",
      gridTemplateAreas:
      `
      "panel board"
      `
    }
  },
  panel: {
    gridArea: "panel",
    display: "flex",
    flexDirection: "column",
    height: "100vh",
    alignItems: "center",
    justifyContent: "center",
    [theme.breakpoints.down("sm")]: {
      height: "25vh",
    }
  },
  board: {
    gridArea: "board",
    display: "flex",
    height: "100vh",
    alignItems: "center",
    justifyContent: "center",
    [theme.breakpoints.down("sm")]: {
      height: "75vh",
      margin: "5px"
    }
  },
  cards: {
    width: "500px"
  }
}));

function App() {
  const classes = useStyles();

  // State Hooks
  const [name1, setName1] = useState("")
  const [img1, setImg1] = useState("")
  const [keywords1, setKeywords1] = useState([])
  
  const [name2, setName2] = useState("")
  const [img2, setImg2] = useState("")
  const [keywords2, setKeywords2] = useState([])
  
  const [name3, setName3] = useState("")
  const [img3, setImg3] = useState("")
  const [keywords3, setKeywords3] = useState([])

  function handleBoard(data) {
    if (data.getRandomTarot.length > 1) {
      setName1(data.getRandomTarot[0].name)
      setImg1(data.getRandomTarot[0].img)
      setKeywords1(data.getRandomTarot[0].keywords)
      
      setName2(data.getRandomTarot[1].name)
      setImg2(data.getRandomTarot[1].img)
      setKeywords2(data.getRandomTarot[1].keywords)
      
      setName3(data.getRandomTarot[2].name)
      setImg3(data.getRandomTarot[2].img)
      setKeywords3(data.getRandomTarot[2].keywords)
    
    } else { 
      setName1("")
      setImg1("")
      setKeywords1([])
      
      setName2("")
      setImg2("")
      setKeywords2([])
      
      setName3(data.getRandomTarot[0].name)
      setImg3(data.getRandomTarot[0].img)
      setKeywords3(data.getRandomTarot[0].keywords)
    }
  }

  // console.log(name1)
  // console.log(img1)
  // console.log(name2)
  // console.log(img2)
  // console.log(name3)
  // console.log(img3)

  return (
  <ApolloProvider client={client}>
    <div className={classes.root}>
      <div className={classes.panel}>
      <Panel 
        className={classes.buttons}
        onPanelChange={handleBoard} 
      />
      </div>
      <div className={classes.board}>
        <TarotCards
          name={name1}
          img={img1}
          keywords={keywords1}
          />
        <TarotCards
          name={name2}
          img={img2}
          keywords={keywords2}
          />
        <TarotCards
          name={name3}
          img={img3}
          keywords={keywords3}
        />
      </div>

    </div>
  </ApolloProvider>
  );
}

export default App