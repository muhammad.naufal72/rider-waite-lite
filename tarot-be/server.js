const express = require("express")
const PORT = 6969;
const {graphqlHTTP} = require("express-graphql");
const schema = require('./Schemas')
const cors = require('cors')


const app = express();

app.use(cors())
app.use('/graphql', graphqlHTTP({
    schema,
    graphiql: true
}))
// history

app.listen(PORT, () => {
    console.log("Server running...")
})