import {gql} from '@apollo/client'

export const PULL_1= gql`
    query {
        getRandomTarot(num:1){
            name,
            img,
            keywords
        }
    }
`;

export const PULL_3= gql`
    query {
        getRandomTarot(num:3){
            name,
            img,
            keywords
        }
    }
`;