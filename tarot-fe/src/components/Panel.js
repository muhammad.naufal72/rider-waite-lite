/* eslint-disable react/prop-types */
import {React, useEffect, useState} from 'react';
import { useLazyQuery, gql } from '@apollo/client';
import { PULL_1, PULL_3 } from '../GraphQL/Queries';
import { makeStyles } from '@material-ui/core/styles';
import {Button} from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    buttons: {
      margin: "5px",
      display: "flex",
      direction: "column"
    },
  }));

function Panel({onPanelChange}) {
  const classes = useStyles()

  const [get1RandomTarot, {loading: loading1, data: data1}] = useLazyQuery(PULL_1, {fetchPolicy: "network-only"})
  const [get3RandomTarot, {loading: loading3, data: data3}] = useLazyQuery(PULL_3, {fetchPolicy: "network-only"})

  
  useEffect(() => {
    if(data1) {
      onPanelChange(data1)
      console.log(data1)
    }
  }, [loading1])

  useEffect(() => {
    if(data3) {
      onPanelChange(data3)
      console.log(data3)
    }
  }, [loading3])
  
  if (loading1 || loading3) {
    return <p>Loading...</p>
  }

  return(
    <div>
      <Button 
        onClick={() => {
          get1RandomTarot()
        }}
        variant="contained" 
        color="primary" 
        className={classes.buttons}>
            Pull 1
      </Button>
      <Button 
        onClick={() => {
          get3RandomTarot()
        }}
        variant="contained" 
        color="primary" 
        className={classes.buttons}>
            Pull 3
      </Button>
    </div>
  );
}

export default Panel;