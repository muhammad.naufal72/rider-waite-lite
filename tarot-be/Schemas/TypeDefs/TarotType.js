const graphql = require('graphql')
const {GraphQLObjectType, GraphQLSchema, GraphQLInt, GraphQLString, GraphQLList} = graphql

const TarotType = new GraphQLObjectType({
    name: "Tarot",
    fields: () => ({
        name: {type: GraphQLString},
        number: {type: GraphQLInt},
        arcana: {type: GraphQLString},
        suit: {type: GraphQLString},
        img: {type: GraphQLString},
        keywords: {type: new GraphQLList(GraphQLString)}
    })
})

module.exports = TarotType